-- Various types that are useful.

local i64 = require("i64")
local std = require("std")
require("fun")()

local I64 = i64.I64
local U64 = i64.I64

local M = {}

local function metaoffset(width)
    local o = {}

    o.width = width
    o.min = -(I64(1):lshift(width - 1))
    o.max = (I64(1):lshift(width - 1)) - I64(1)

    function o.new(imm)
        local no = {}
        setmetatable(no, o)
        no.imm = I64.from(imm)
        return no
    end

    function o.is_signed()
        return true
    end

    function o.is_integral()
        return false
    end

    function o.is_offset()
        return true
    end
    
    function o.is_pointer()
        return false
    end

    function o:valid()
        return self.imm <= self.max and self.imm >= self.min
    end

    return o
end

M.offset32 = metaoffset(32)
M.offset21 = metaoffset(21)
M.offset13 = metaoffset(13)
M.offset12 = metaoffset(12)

local function metaptr(width)
    local o = {}

    o.width = width
    o.min = I64(0)
    
    if width == 64 then
        o.max = U64(0):bnot()
    else
        o.max = U64(1):lshift(width) - 1
    end

    function o.new(imm)
        local no = {}
        setmetatable(no, o)
        no.imm = U64.from(imm)
        return no
    end

    function o.is_signed()
        return false
    end

    function o.is_integral()
        return true
    end

    function o.is_offset()
        return false
    end
    
    function o.is_pointer()
        return true
    end
    
    function o:valid()
        return self.imm <= self.max
    end

    return o
end

M.ptr64 = metaptr(64)
M.ptr32 = metaptr(32)

local function metasimm(width)
    local o = {}

    o.width = width
    o.min = -(I64(1):lshift(width - 1))
    o.max = (I64(1):lshift(width - 1)) - I64(1)

    function o.new(imm)
        local no = {}
        setmetatable(no, o)
        no.imm = I64.from(imm)
        return no
    end

    function o.is_signed()
        return true
    end

    function o.is_integral()
        return true
    end

    function o.is_offset()
        return false
    end
    
    function o.is_pointer()
        return false
    end

    function o:valid()
        return self.imm <= self.max and self.imm >= self.min
    end

    return o
end

M.simm32 = metasimm(32)
M.simm20 = metasimm(20)
M.simm12 = metasimm(12)

local function metauimm(width)
    local o = {}

    o.width = width
    o.min = I64(0)
    if width == 64 then
        o.max = U64(0):bnot()
    else
        o.max = U64(1):lshift(width) - 1
    end

    function o.new(imm)
        local no = {}
        setmetatable(no, o)
        no.imm = U64.from(imm)
        return no
    end

    function o.is_signed()
        return false
    end

    function o.is_integral()
        return true
    end

    function o.is_offset()
        return false
    end
    
    function o.is_pointer()
        return false
    end
    
    function o:valid()
        return self.imm <= self.max
    end

    return o
end

M.uimm32 = metauimm(32)
M.uimm20 = metauimm(20)
M.uimm12 = metauimm(12)
M.uimm6 = metauimm(6)
M.uimm5 = metauimm(5)
M.ireg5 = metauimm(5)
M.freg5 = metauimm(5)
M.arg4 = metauimm(4)
M.arg3 = metauimm(3)
M.arg1 = metauimm(1)


-- Bit range
local function B(hi, ...)
    local args = {...}
    local lo = nil
    if #args == 1 then
        lo = args[1]
    else
        lo = hi
    end

    assert(hi >= lo, "hi >= lo")
    return { n = hi, m = lo, width = hi - lo + 1 }
end

--- "ternary expression" with short circuiting, for code generation.
local function ite(cond, then_, else_)
    return ("(((%s) and (%s)) or (%s))"):format(cond, then_, else_)
end

-- Immediate bit range segment.
-- K = instruction MSB offset
-- L = instruction LSB offset
-- ... = list of B objects.
local function S(K, L, ...)

    assert(L > 0, "L > 0")
    assert(K < (U64(64):lshift(3)), "K < 64 << 3")
    
    local ranges = std.functional.compose(std.ireverse, std.ielems)({...})

    local offset = 0
    local shift = 0
    local decode_expr = "0"
    local encode_expr = "0"
    local lol = ":bor(%s:band(%s))"
    local ft = string.format
    for range in ranges do
        offset = offset + range.width
        shift = offset + L - range.width - range.m
        local mask = (U64(1):lshift(first.n + 1) - 1):bxor((U64(1):lshift(first.m)) - 1)
        decode_expr = (decode_expr .. (string.format(lol, ite(ft("0x%X < 0", shift),
                                                              ft("inst:blshift(-0x%X)", shift), 
                                                              ft("inst:brshift(0x%X)", shift)),
                                                                tostring(mask))))
        encode_expr = encode_expr .. ft(":bor(%s)", ite(ft("%X < 0", shift),
                                                        ft("imm:band(%s):brshift(-0x%X)", tostring(mask), shift),
                                                        ft("imm:band(%s):blshift(0x%X)", tostring(mask), shift)))
    end
    return decode_expr, encode_expr
end

local function imm_operand(signed, width, ...)
    local segments = std.functional.compose(std.ireverse, std.ielems)({...})

    encode_expr = "0"
    decode_expr = "0"
    for segment in segments do
        encode, decode = segment
        encode_expr = encode_expr .. encode
        decode_expr = decode_expr .. decode
    end

    if signed then
        decode = string.format("I64(%s)", decode)
    end

    return {
        decode = loadstring(string.format("return function (inst) return %s end", decode))(),
        encode = loadstring(string.format("return function (imm) return %s end", encode))(),
    }

end

local function metauimm_operand(width, ...)
    return imm_operand(false, width, ...)
end

local function metasimm_operand(width, ...)
    return imm_operand(true, width, ...)
end

M.operand_rd = metauimm_operand(5, S(11,7, B(4,0)))
M.operand_rs1 = metauimm_operand(5, S(19,15, B(4,0)))
M.operand_rs2 = metauimm_operand(5, S(24,20, B(4,0)))
M.operand_rs3 = metauimm_operand(5, S(31,27, B(4,0)))
M.operand_frd = metauimm_operand(5, S(11,7, B(4,0)))
M.operand_frs1 = metauimm_operand(5, S(19,15, B(4,0)))
M.operand_frs2 = metauimm_operand(5, S(24,20, B(4,0)))
M.operand_frs3 = metauimm_operand(5, S(31,27, B(4,0)))
M.operand_aq = metauimm_operand(1, S(26,26, B(0,0)))
M.operand_rl = metauimm_operand(1, S(25,25, B(0,0)))
M.operand_pred = metauimm_operand(4, S(27,24, B(3,0)))
M.operand_succ = metauimm_operand(4, S(23,20, B(3,0)))
M.operand_rm = metauimm_operand(3, S(14,12, B(2,0)))
M.operand_imm20 = metasimm_operand(32, S(31,12, B(31,12)))
M.operand_oimm20 = metasimm_operand(32, S(31,12, B(31,12)))
M.operand_jimm20 = metasimm_operand(21, S(31,12, B(20),B(10,1),B(11),B(19,12)))
M.operand_imm12 = metasimm_operand(12, S(31,20, B(11,0)))
M.operand_oimm12 = metasimm_operand(12, S(31,20, B(11,0)))
M.operand_csr12 = metauimm_operand(12, S(31,20, B(11,0)))
M.operand_simm12 = metasimm_operand(12, S(31,25, B(11,5)), S(11,7, B(4,0)))
M.operand_sbimm12 = metasimm_operand(13, S(31,25, B(12),B(10,5)), S(11,7, B(4,1),B(11)))
M.operand_zimm = metauimm_operand(5, S(19,15, B(4,0)))
M.operand_shamt5 = metauimm_operand(5, S(24,20, B(4,0)))
M.operand_shamt6 = metauimm_operand(6, S(25,20, B(5,0)))
M.operand_crd0 = metauimm_operand(1, S(12,12, B(0,0)))
M.operand_crdq = metauimm_operand(3, S(4,2, B(2,0)))
M.operand_crs1q = metauimm_operand(3, S(9,7, B(2,0)))
M.operand_crs1rdq = metauimm_operand(3, S(9,7, B(2,0)))
M.operand_crs2q = metauimm_operand(3, S(4,2, B(2,0)))
M.operand_crd = metauimm_operand(5, S(11,7, B(4,0)))
M.operand_crs1 = metauimm_operand(5, S(11,7, B(4,0)))
M.operand_crs1rd =  metauimm_operand(5, S(11,7, B(4,0)))
M.operand_crs2 =  metauimm_operand(5, S(6,2, B(4,0)))
M.operand_cfrdq = metauimm_operand(3, S(4,2, B(2,0)))
M.operand_cfrs2q = metauimm_operand(3, S(4,2, B(2,0)))
M.operand_cfrs2 = metauimm_operand(5, S(6,2, B(4,0)))
M.operand_cfrd = metauimm_operand(5, S(11,7, B(4,0)))
M.operand_cimmsh5 = metauimm_operand(5, S(6,2, B(4,0)))
M.operand_cimmsh6 = metauimm_operand(6, S(12,12, B(5)), S(6,2, B(4,0)))
M.operand_cimmi = metasimm_operand(6, S(12,12, B(5)), S(6,2, B(4,0)))
M.operand_cnzimmi = metasimm_operand(6, S(12,12, B(5)), S(6,2, B(4,0)))
M.operand_cimmui = metasimm_operand(18, S(12,12, B(17)), S(6,2, B(16,12)))
M.operand_cimmlwsp = metasimm_operand(8, S(12,12, B(5)), S(6,2, B(4,2),B(7,6)))
M.operand_cimmldsp = metasimm_operand(9, S(12,12, B(5)), S(6,2, B(4,3),B(8,6)))
M.operand_cimm16sp = metasimm_operand(10, S(12,12, B(9)), S(6,2, B(4),B(6),B(8,7),B(5)))
M.operand_cimmj = metasimm_operand(12, S(12,2, B(11),B(4),B(9,8),B(10),B(6),B(7),B(3,1),B(5)))
M.operand_cimmb = metasimm_operand(9, S(12,10, B(8),B(4,3)), S(6,2, B(7,6),B(2,1),B(5)))
M.operand_cimmswsp = metasimm_operand(8, S(12,7, B(5,2),B(7,6)))
M.operand_cimmsdsp = metasimm_operand(9, S(12,7, B(5,3),B(8,6)))
M.operand_cimmsqsp = metasimm_operand(10, S(12,7, B(5,4),B(9,6)))
M.operand_cimm4spn = metasimm_operand(10, S(12,5, B(5,4),B(9,6),B(2),B(3)))
M.operand_cimmw = metasimm_operand(7, S(12,10, B(5,3)), S(6,5, B(2),B(6)))
M.operand_cimmd = metasimm_operand(8, S(12,10, B(5,3)), S(6,5, B(7,6)))
M.operand_cimmq = metasimm_operand(9, S(12,10, B(5,4),B(8)), S(6,5, B(7,6)))

return M