# lua-rvemu

An implementation of a RISC-V system in Lua. Primarily for use in game mods.

# License

Apache-2.0
