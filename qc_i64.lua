i64 = require "i64"

LH1 = {int(0xFFFFFFFF), int(0xFFFFFFFF)}
LH1BIT = {int(0xFFFFFFFF), int(0xFFFFFFFF), int(63)}
LBIT = {int(0xFFFFFFFF), int(31)}
ASTROSHIFT = {int(0xFFFFFFFF), int(0xFFFFFFFF), int(64, 0xFFFFFFFF)}
LH2 = {
	int(0xFFFFFFFF),
	int(0xFFFFFFFF),
	int(0xFFFFFFFF),
	int(0xFFFFFFFF),
}

-- Constructor tests

property "Consistent construction / equality" {
	generators = LH1,
	check = function(l, h)
		local a, b = i64.I64.new(l, h), i64.I64.new(l, h)
		return a:equal(b) and a.low == b.low and a.high == b.high
	end,
}

property "Consistent from" {
	generators = {int(0xFFFFFFFF)},
	check = function(l)
		local a, b = i64.I64(l), i64.I64.new(l, 0)
		return a:equal(b) and a.low == b.low and a.high == b.high
	end,
}

-- Arithmetic laws

NEGONE = i64.I64(-1)
ZERO = i64.I64(0)
ONE = i64.I64(1)
TWO = i64.I64(2)

property "A + 0 = A" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return a:add(ZERO):equal(a)
	end,
}

property "A - 0 = 0" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return a:sub(ZERO):equal(a)
	end,
}

property "A - A = 0" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return a:sub(a):equal(ZERO)
	end,
}

property "A + B - B = A" {
	generators = LH2,
	check = function(l1, h1, l2, h2)
		local a, b = i64.I64.new(l1, h1), i64.I64.new(l2, h2)
		return a:add(b):sub(b):equal(a)
	end,
}

property "A * 0 = 0" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return a:mul(ZERO):equal(ZERO)
	end,
}

property "A * 1 = A" {
	generators = LH1,
	check = function(l,h)
		local a = i64.I64.new(l, h)
		return a:mul(ONE):equal(a)
	end,
}

property "A * 2 = A + A" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return a:mul(TWO):equal(a:add(a))
	end,
}

property "A / B = Q rem R <-> B * Q + R = A" {
	generators = LH2,
	check = function(l1, h1, l2, h2)
		local a, b = i64.I64.new(l1, h1), i64.I64.new(l2, h2)
		local q, r = a:div(b)
		return b:mul(q):add(r):equal(a)
	end,
}


property "A / A = 1 rem 0 \\/ A = 0 <-> (Q = 0) /\\ (R = 0)" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		local q, r = a:div(a)
		if a:equal(ZERO) then
			return q:equal(ZERO) and r:equal(ZERO)
		end
		return q:equal(ONE) and r:equal(ZERO)
	end,
}

--[[
TODO: Ridiculously large divisions can cause the long division algorithm to stumble due to loss of a 65th bit

property "A / B >= 1 <-> A >= B" {
	generators = LH2,
	check = function(l1, h1, l2, h2)
		local a, b = i64.I64.new(l1, h1), i64.I64.new(l2, h2)
		local q, r = a:div(b)
		if q:above(ONE) or q:equal(ONE) then
			return a:above(b) or a:equal(b)
		else
			return a:below(b)
		end
	end,
}
]]

-- Bitwise

property "~~A = A" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return a:bnot():bnot():equal(a)
	end,
}

property "BSET(A, n) >= A" {
	generators = LH1BIT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		local b = a:bset(n)
		return b:above(a) or b:equal(a)
	end,
}

property "BCLEAR(A, n) <= A" {
	generators = LH1BIT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		local b = a:bclear(n)
		return b:below(a) or b:equal(a)
	end,
}

property "BIT(a, n) in (0, 1)" {
	generators = LH1BIT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		local b = a:bit(n)
		return b == 0 or b == 1
	end,
}

property "IS_NEGATIVE(a) == BIT(a, 63)" {
	generators = LH1BIT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		if a:is_negative() then
			return a:bit(63) == 1
		else
			return a:bit(63) == 0
		end
	end,
}

property "A & B <= MAX(A, B)" {
	generators = LH2,
	check = function(l1, h1, l2, h2)
		local a, b = i64.I64.new(l1, h1), i64.I64.new(l2, h2)
		local c, d = a:band(b), a:max(b)
		return c:below(d) or c:equal(d)
	end,
}

property "A | B >= MAX(A, B)" {
	generators = LH2,
	check = function(l1, h1, l2, h2)
		local a, b = i64.I64.new(l1, h1), i64.I64.new(l2, h2)
		local c, d = a:bor(b), a:max(b)
		return c:above(d) or c:equal(d)
	end,
}

property "A ^ 0 = A" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return a:bxor(ZERO):equal(a)
	end,
}

property "PARITY(A) != PARITY(A ^ 1)" {
	generators = LH1,
	check = function(l, h)
		local a = i64.I64.new(l, h)
		return not a:bxor(ONE):equal(a)
	end,
}

property "A ^ B = A <-> (A = 0) \\/ (B = 0)" {
	generators = LH2,
	check = function(l1, h1, l2, h2)
		local a, b = i64.I64.new(l1, h1), i64.I64.new(l2, h2)
		if a:equal(ZERO) or b:equal(ZERO) then
			return a:bxor(b):equal(a:max(b))
		else
			local c = a:bxor(b)
			return not (a:equal(c) or b:equal(c))
		end
	end,
}

--[[
TODO: Precision loss causes this one to fail too much

property "i64.lshift(a, n) & 0xFFFFFFFF = bit.lshift(a, n)" {
	generators = LBIT,
	check = function(a, n)
		return i64.bit.band(i64.lshift(a, n), 0xFFFFFFFF) == i64.bit.lshift(a, n)
	end,
}
]]

property "i64.rshift(a, n) & 0xFFFFFFFF = bit.rshift(a, n)" {
	generators = LBIT,
	check = function(a, n)
		return i64.bit.band(i64.rshift(a, n), 0xFFFFFFFF) == i64.bit.rshift(a, n)
	end,
}

property "A >> n <= A" {
	generators = LH1BIT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		local b = a:brshift(n)
		return b:below(a) or b:equal(a)
	end,
}

property "n >= 64 -> A << n = 0" {
	generators = ASTROSHIFT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		return a:blshift(n):equal(ZERO)
	end,
}

property "n >= 64 -> A >> n = 0" {
	generators = ASTROSHIFT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		return a:brshift(n):equal(ZERO)
	end,
}

property "n >= 64 -> A >>> n = (IS_NEGATIVE(a) ? -1 : 0)" {
	generators = ASTROSHIFT,
	check = function(l, h, n)
		local a = i64.I64.new(l, h)
		local b = a:barshift(n)
		if a:is_negative() then
			return b:equal(NEGONE)
		else
			return b:equal(ZERO)
		end
	end,
}
