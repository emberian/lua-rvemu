-- Instruction decoding

--local types = require "types"
local meta = require "meta"
local M = {}

--[[ Decode none --]]
function M.decode_none(dec, inst)
	dec.rd = meta.riscv_ireg_zero
    dec.rs1 = meta.riscv_ireg_zero
    dec.rs2 = meta.riscv_ireg_zero 
	dec.imm = 0
end

--[[ Decode C nop --]]
function M.decode_ci_none(dec, inst)

	dec.rd = meta.riscv_ireg_zero
    dec.rs1 = meta.riscv_ireg_zero
    dec.rs2 = meta.riscv_ireg_zero
	dec.imm = 0
end

--[[ Decode CR --]]
function M.decode_cr(dec, inst)
	dec.rd = types.operand_crs1rd.decode(inst)
    dec.rs1 = types.operand_crs1rd.decode(inst)
	dec.rs2 = types.operand_crs2.decode(inst)
	dec.imm = 0
end

--[[ Decode CR mv --]]
function M.decode_cr_mv(dec, inst)

	dec.rd = types.operand_crd.decode(inst)
	dec.rs1 = meta.riscv_ireg_zero
	dec.rs2 = types.operand_crs2.decode(inst)
	dec.imm = 0
end

--[[ Decode CR jalr --]]
function M.decode_cr_jalr(dec, inst)

	dec.rd = meta.riscv_ireg_ra
	dec.rs1 = types.operand_crs1.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = 0
end

--[[ Decode CR jr --]]
function M.decode_cr_jr(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = types.operand_crs1.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = 0
end

--[[ Decode CI --]]
function M.decode_ci(dec, inst)

	dec.rd = types.operand_crs1rd.decode(inst)
    dec.rs1 = types.operand_crs1rd.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmi.decode(inst)
end

--[[ Decode CI shamt5 --]]
function M.decode_ci_sh5(dec, inst)

	dec.rd = types.operand_crs1rd.decode(inst)
    dec.rs1 = types.operand_crs1rd.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmsh5.decode(inst)
end

--[[ Decode CI shamt6 --]]
function M.decode_ci_sh6(dec, inst)

	dec.rd = types.operand_crs1rd.decode(inst)
    dec.rs1 = types.operand_crs1rd.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmsh6.decode(inst)
end

--[[ Decode CI li --]]
function M.decode_ci_li(dec, inst)

	dec.rd = types.operand_crd.decode(inst)
	dec.rs1 = meta.riscv_ireg_zero
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmi.decode(inst)
end

--[[ Decode CI lui --]]
function M.decode_ci_lui(dec, inst)

	dec.rd = types.operand_crd.decode(inst)
	dec.rs1 = meta.riscv_ireg_zero
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmui.decode(inst)
end

--[[ Decode CI lwsp --]]
function M.decode_ci_lwsp(dec, inst)

	dec.rd = types.operand_crd.decode(inst)
	dec.rs1 = meta.riscv_ireg_sp
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmlwsp.decode(inst)
end

--[[ Decode CI ldsp --]]
function M.decode_ci_ldsp(dec, inst)

	dec.rd = types.operand_crd.decode(inst)
	dec.rs1 = meta.riscv_ireg_sp
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmldsp.decode(inst)
end

--[[ Decode CI 16sp --]]
function M.decode_ci_16sp(dec, inst)

	dec.rd = meta.riscv_ireg_sp
	dec.rs1 = meta.riscv_ireg_sp
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimm16sp.decode(inst)
end

--[[ Decode CSS swsp --]]
function M.decode_css_swsp(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = meta.riscv_ireg_sp
	dec.rs2 = types.operand_crs2.decode(inst)
	dec.imm = types.operand_cimmswsp.decode(inst)
end

--[[ Decode CSS sdsp --]]
function M.decode_css_sdsp(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = meta.riscv_ireg_sp
	dec.rs2 = types.operand_crs2.decode(inst)
	dec.imm = types.operand_cimmsdsp.decode(inst)
end

--[[ Decode CIW 4spn --]]
function M.decode_ciw_4spn(dec, inst)

	dec.rd = types.operand_crdq.decode(inst) + 8
	dec.rs1 = meta.riscv_ireg_sp
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimm4spn.decode(inst)
end

--[[ Decode CL lw --]]
function M.decode_cl_lw(dec, inst)

	dec.rd = types.operand_crdq.decode(inst) + 8
	dec.rs1 = types.operand_crs1q.decode(inst) + 8
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmw.decode(inst)
end

--[[ Decode CL ld --]]
function M.decode_cl_ld(dec, inst)

	dec.rd = types.operand_crdq.decode(inst) + 8
	dec.rs1 = types.operand_crs1q.decode(inst) + 8
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmd.decode(inst)
end

--[[ Decode CS f --]]
function M.decode_cs(dec, inst)

	dec.rd = types.operand_crs1rdq.decode(inst) + 8
    dec.rs1 = types.operand_crs1rdq.decode(inst) + 8
	dec.rs2 = types.operand_crs2q.decode(inst) + 8
	dec.imm = 0
end

--[[ Decode CS sd --]]
function M.decode_cs_sd(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = types.operand_crs1q.decode(inst) + 8
	dec.rs2 = types.operand_crs2q.decode(inst) + 8
	dec.imm = types.operand_cimmd.decode(inst)
end

--[[ Decode CS sw --]]
function M.decode_cs_sw(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = types.operand_crs1q.decode(inst) + 8
	dec.rs2 = types.operand_crs2q.decode(inst) + 8
	dec.imm = types.operand_cimmw.decode(inst)
end

--[[ Decode CB --]]
function M.decode_cb(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = types.operand_crs1q.decode(inst) + 8
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmb.decode(inst)
end

--[[ Decode CB imm --]]
function M.decode_cb_imm(dec, inst)

	dec.rd = types.operand_crs1rdq.decode(inst) + 8
    dec.rs1 = types.operand_crs1rdq.decode(inst) + 8
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmi.decode(inst)
end

--[[ Decode CB shamt5 --]]
function M.decode_cb_sh5(dec, inst)

	dec.rd = types.operand_crs1rdq.decode(inst) + 8
    dec.rs1 = types.operand_crs1rdq.decode(inst) + 8
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmsh5.decode(inst)
end

--[[ Decode CB shamt6 --]]
function M.decode_cb_sh6(dec, inst)

	dec.rd = types.operand_crs1rdq.decode(inst) + 8
    dec.rs1 = types.operand_crs1rdq.decode(inst) + 8
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmsh6.decode(inst)
end

--[[ Decode CJ --]]
function M.decode_cj(dec, inst)

	dec.rd = meta.riscv_ireg_zero
    dec.rs1 = meta.riscv_ireg_zero
    dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmj.decode(inst)
end

--[[ Decode CJ jal --]]
function M.decode_cj_jal(dec, inst)

	dec.rd = meta.riscv_ireg_ra
	dec.rs1 = meta.riscv_ireg_zero
    dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_cimmj.decode(inst)
end

--[[ Decode R --]]
function M.decode_r(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = types.operand_rs2.decode(inst)
	dec.imm = 0
end

--[[ Decode R RM --]]
function M.decode_r_m(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = types.operand_rs2.decode(inst)
	dec.imm = 0
	dec.rm = types.operand_rm.decode(inst)
end

--[[ Decode R AMO L --]]
function M.decode_r_l(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = 0
	dec.aq = types.operand_aq.decode(inst)
	dec.rl = types.operand_rl.decode(inst)
end

--[[ Decode R AMO S --]]
function M.decode_r_a(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = types.operand_rs2.decode(inst)
	dec.imm = 0
	dec.aq = types.operand_aq.decode(inst)
	dec.rl = types.operand_rl.decode(inst)
end

--[[ Decode R 4f --]]
function M.decode_r4_m(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = types.operand_rs2.decode(inst)
	dec.rs3 = types.operand_rs3.decode(inst)
	dec.imm = 0
	dec.rm = types.operand_rm.decode(inst)
end

--[[ Decode R fence --]]
function M.decode_r_f(dec, inst)

	dec.rd = meta.riscv_ireg_zero
    dec.rs1 = meta.riscv_ireg_zero
    dec.rs2 = meta.riscv_ireg_zero
	dec.pred = types.operand_pred.decode(inst)
	dec.succ = types.operand_succ.decode(inst)
	dec.imm = 0
end

--[[ Decode I --]]
function M.decode_i(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_imm12.decode(inst)
end

--[[ Decode I sh5 --]]
function M.decode_i_sh5(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_shamt5.decode(inst)
end

--[[ Decode I sh6 --]]
function M.decode_i_sh6(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_shamt6.decode(inst)
end

--[[ Decode S Store --]]
function M.decode_s(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = types.operand_rs2.decode(inst)
	dec.imm = types.operand_simm12.decode(inst)
end

--[[ Decode SB Branch --]]
function M.decode_sb(dec, inst)

	dec.rd = meta.riscv_ireg_zero
	dec.rs1 = types.operand_rs1.decode(inst)
	dec.rs2 = types.operand_rs2.decode(inst)
	dec.imm = types.operand_sbimm12.decode(inst)
end

--[[ Decode U --]]
function M.decode_u(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = meta.riscv_ireg_zero
    dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_imm20.decode(inst)
end

--[[ Decode UJ --]]
function M.decode_uj(dec, inst)

	dec.rd = types.operand_rd.decode(inst)
	dec.rs1 = meta.riscv_ireg_zero
    dec.rs2 = meta.riscv_ireg_zero
	dec.imm = types.operand_jimm20.decode(inst)
end

return M