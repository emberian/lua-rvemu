local i64 = {}

repeat
	if bit == nil then
		if bit32 ~= nil then
			bit = bit32
			break
		end
		local success, slowbit = pcall(require, "slowbit")
		if success then
			bit = slowbit
			break
		end
		error("No bit manipulation library available")
	end
until true

i64.bit = bit

local ONES32 = 0xFFFFFFFF
local SIGN_BIT = 0x80000000
local NONSIGN_BITS = 0x7FFFFFFF  -- Not safe to assume this is bnot(SIGN_BIT)
local TWO32 = 0x100000000

local I64 = {cache = {}}
i64.I64 = I64

-- NB: Modulus converts to a positive number, as desired
function i64._norm_u32(i)
	return bit.band(ONES32, i) % TWO32
end

-- NB: Could probably be done without using 33 bits
function i64._norm_i32(i)
	i = i64._norm_u32(i)
	if bit.band(SIGN_BIT, i) ~= 0 then
		i = i - TWO32
	end
	return i
end

-- NB: Big endian
function i64._u32_bitstring(i)
	local res = ""
	for b = 0, 31 do
		if bit.band(SIGN_BIT, bit.lshift(i, b)) ~= 0 then
			res = res .. "1"
		else
			res = res .. "0"
		end
	end
	return res
end

-- Slow, but correct bit shifts that work with >32 bit integers
-- Note that the implementation defines the upper limit on how many
-- bits of precision are available. (Most of this library assumes at
-- least 53 bits.)
function i64.lshift(val, amt)
	return val * (2 ^ amt)
end

function i64.rshift(val, amt)
	return math.floor(val / (2 ^ amt))
end

function i64._print_table(t, l)
	if l == nil then l = 0 end
	if type(t) ~= "table" then
		print(type(t), t)
		return
	end
	for k, v in pairs(t) do
		if type(v) == "table" then
			print(string.rep('\t', l), k, '=', '{')
			i64._print_table(v, l + 1)
			print(string.rep('\t', l), '}')
		else
			print(string.rep('\t', l), k, '=', v)
		end
	end
end

local function _make_bfunc(bfunc)
	return function(self, ...)
		local others = {...}
		local self = I64.from(self)
		local low, high = self.low, self.high

		for i, other in ipairs(others) do
			local oth = I64.from(other)
			low = bfunc(low, oth.low)
			high = bfunc(high, oth.high)
		end

		return I64.new(low, high)
	end
end

i64.band = _make_bfunc(bit.band)
i64.bor = _make_bfunc(bit.bor)
i64.bxor = _make_bfunc(bit.bxor)

I64.band = i64.band
I64.bor = i64.bor
I64.bxor = i64.bxor

function I64.new(low, high)
	assert(type(low) == "number", type(low))
	assert(type(high) == "number", type(high))
	return setmetatable({low = i64._norm_u32(low), high = i64._norm_u32(high)}, I64._metatable)
end

-- NB: Depending on safe conversion in the constructor due to _norm_u32
function I64.from(i, high)
	if type(i) == "table" then
		if getmetatable(i) == I64._metatable then
			return i
		end
		error("Attempt to cast non-I64 " .. tostring(i) .. " to I64")
	end
	if high == nil then
		high = 0
	end
	return I64.new(i, high + i64.rshift(i, 32))
end

function I64.from_cache(i)
	local v = I64.cache[i]
	if v ~= nil then
		return v
	end
	v = I64.from(i)
	I64.cache[i] = v
	return v
end

-- Formatting

function I64:tostring()
	return "I64(" .. self.low .. ", " .. self.high .. ")"
end

function I64:tobitstring()
	return i64._u32_bitstring(self.high) .. i64._u32_bitstring(self.low)
end

-- Bitwise operations

function I64:bnot()
	return I64.new(bit.bnot(self.low), bit.bnot(self.high))
end


function I64:blshift(amt)
	amt = I64.from(amt)
	amt = amt.low
	if amt >= 64 then
		return I64.new(0, 0)
	end
	if amt >= 32 then
		return I64.new(0, bit.lshift(self.low, amt - 32))
	end
	local upval = bit.rshift(bit.band(ONES32, i64.lshift(ONES32, 32 - amt), self.low) % TWO32, 32 - amt)
	return I64.new(bit.lshift(self.low, amt), bit.bor(bit.lshift(self.high, amt), upval))
end

function I64:brshift(amt)
	amt = I64.from(amt)
	amt = amt.low
	if amt >= 64 then
		return I64.new(0, 0)
	end
	if amt >= 32 then
		return I64.new(bit.rshift(self.high, amt-32), 0)
	end
	local downval = bit.lshift(bit.band(ONES32, bit.rshift(ONES32, 32 - amt), self.high) % TWO32, 32 - amt)
	return I64.new(bit.bor(bit.rshift(self.low, amt), downval), bit.rshift(self.high, amt))
end

function I64:is_negative()
	return bit.band(SIGN_BIT, self.high) ~= 0
end

function I64:barshift(amt)
	amt = I64.from(amt)
	amt = amt.low
	local sgn = 0
	if self:is_negative() then
		sgn = ONES32
	end
	if amt >= 64 then
		return I64.new(sgn, sgn)
	end
	if amt >= 32 then
		return I64.new(bit.arshift(self.high, amt - 32), sgn)
	end
	local downval = bit.lshift(bit.band(ONES32, i64.rshift(ONES32, 32 - amt), self.high) % TWO32, 32 - amt)
	return I64.new(bit.bor(bit.rshift(self.low, amt), downval), bit.arshift(self.high, amt))
end

function i64._bool_to_int(b)
	if b then return 1 else return 0 end
end

function I64:bit(n)
	n = I64.from(n)
	n = n.low
	if n >= 64 then
		return 0
	end
	if n >= 32 then
		return i64._bool_to_int(bit.band(bit.lshift(1, n - 32), self.high) ~= 0)
	end
	return i64._bool_to_int(bit.band(bit.lshift(1, n), self.low) ~= 0)
end

function I64:bset(n)
	n = I64.from(n)
	n = n.low
	local low, high = self.low, self.high
	if n >= 32 then
		high = bit.bor(high, bit.lshift(1, n - 32))
	else
		low = bit.bor(low, bit.lshift(1, n))
	end
	return I64.new(low, high)
end

function I64:bclear(n)
	n = I64.from(n)
	n = n.low
	local low, high = self.low, self.high
	if n >= 32 then
		high = bit.band(high, bit.bnot(bit.lshift(1, n - 32)))
	else
		low = bit.band(low, bit.bnot(bit.lshift(1, n - 32)))
	end
	return I64.new(low, high)
end

-- Comparisons

function I64:equal(other)
	other = I64.from(other)
	return self.low == other.low and self.high == other.high
end

-- Signed:
function I64:greater(other)
	other = I64.from(other)
	if i64._norm_i32(self.high) > i64._norm_i32(other.high) then
		return true
	end
	if self.low > other.low then
		return true
	end
	return false
end

function I64:less(other)
	other = I64.from(other)
	return other:greater(self)
end

-- Unsigned:
function I64:above(other)
	other = I64.from(other)
	if self.high > other.high then
		return true
	end
	if self.high < other.high then
		return false
	end
	if self.low > other.low then
		return true
	end
	return false
end

function I64:below(other)
	other = I64.from(other)
	return other:above(self)
end

-- Basic arithmetic

function I64:add(...)
	local others = {...}
	local low, high = self.low, self.high
	local carry

	for i, other in ipairs(others) do
		other = I64.from(other)
		low = low + other.low
		carry = i64.rshift(low, 32)
		low = bit.band(low, ONES32)

		high = high + carry + other.high
	end

	carry = i64.rshift(high, 32)
	high = bit.band(high, ONES32)
	return I64.new(low, high), I64.from(carry)
end

function I64:sub(other)
	other = I64.from(other)
	return self:add(other:negated())
end

-- NB: This is very expensive!
function I64:mul(other)
	other = I64.from(other)
	local lowres, highres = I64.new(0, 0), I64.new(0, 0)
	local carry, highcarry
	local carryout = I64.new(0, 0)

	for i = 0, 63 do
		if self:bit(i) ~= 0 then
			local lowpart, highpart = other:blshift(i), other:brshift(64 - i)
			lowres, carry = lowres:add(lowpart)
			highres, highcarry = highres:add(highpart, carry)
			carryout = carryout:add(highcarry)
		end
	end

	return lowres, highres, carryout
end

function I64:div(other)
	other = I64.from(other)
	local quot, rem = I64.new(0, 0), I64.new(0, 0)
	if other:equal(0) then
		return quot, rem
	end
	for i = 63, 0, -1 do
		rem = (rem:blshift(1)):bor(self:bit(i))
		if rem:above(other) or rem:equal(other) then
			rem = rem:sub(other)
			quot = quot:bset(i)
		end
	end
	return quot, rem
end

function I64:sdiv(other)
	other = I64.from(other)
	local abself, abother = self:abs(), other:abs()
	local quot, rem = abself:div(abother)
	if (self:is_negative() and not other:is_negative()) or (other:is_negative() and not self:is_negative()) then
		quot = quot:negated()
	end
	return quot, rem
end

function I64:negated()
	return ((self:bnot()):add(1))
end

function I64:abs()
	if self:is_negative() then
		return self:negated()
	end
	return self
end

local function _make_quantile(cmpf)
	return function (self, ...)
		local running = self
		local others = {...}

		for i, other in ipairs(others) do
			other = I64.from(other)
			if cmpf(other, running) then
				running = other
			end
		end

		return running
	end
end

I64.max = _make_quantile(I64.above)
I64.min = _make_quantile(I64.below)
I64.smax = _make_quantile(I64.greater)
I64.smin = _make_quantile(I64.less)

-- Conversion

-- NB: This has a *very* good chance of losing precision in many cases.
function I64:to_u64()
	return self.low + i64.lshift(self.high, 32)
end

-- NB: idem
function I64:to_i64()
	local ipart = self.low + i64.lshift(bit.band(NONSIGN_BITS, self.high), 32)
	if self:is_negative() then
		return (2^64) - ipart
	end
	return ipart
end

-- Accessors for smaller widths

--Unsigned:
function I64:to_u32()
	return self.low
end

function I64:to_u16()
	return bit.band(0xFFFF, self.low)
end

function I64:to_u8()
	return bit.band(0xFF, self.low)
end

--Signed (using that width's sign bit)
function I64:to_i32()
	local v = self:to_u32()
	if bit.band(SIGN_BIT, v) > 0 then
		v = v - TWO32
	end
	return v
end

function I64:to_i16()
	local v = self:to_u16()
	if bit.band(0x8000, v) then
		v = v - 0x10000
	end
	return v
end

function I64:to_i8()
	local v = self:to_u8()
	if bit.band(0x80, v) then
		v = v - 0x100
	end
	return v
end

--Signed (copying from the I64's sign bit)
function I64:to_sc31()
	return bit.bor(bit.band(SIGN_BIT, self.high), bit.band(NONSIGN_BITS, self.low))
end

function I64:to_sc15()
	return bit.bor(bit.lshift(bit.band(SIGN_BIT, self.high), 16), bit.band(0x7FFF, self.low))
end

function I64:to_sc7()
	return bit.bor(bit.lshift(bit.band(SIGN_BIT, self.high), 24), bit.band(0x7F, self.low))
end

I64._metatable = {
	__index = I64,
	__tostring = I64.tostring,
	__tonumber = I64.to_u64,
	__unm = I64.negated,
	__add = I64.add,
	__sub = I64.sub,
	__mul = I64.mul,
	__div = I64.div,
	__eq = I64.equal,
	__lt = I64.below,
	__le = function(self, other) return self:equal(other) or self:below(other) end,
}

I64 = setmetatable(I64, {__call = function(self, ...) return self.from(...) end})

return i64
